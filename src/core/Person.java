package core;

public class Person {
	
	public String name;
	public double pca;
	public String eyeColor;
	public String hairColor;
	public String region;
	
	public Person(String name, 
			double pca, 
			String eyeColor,
			String hairColor,
			String region){
		this.name = name;
		this.pca = pca;
		this.eyeColor = eyeColor;
		this.hairColor = hairColor;
		this.region = region;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", 0.855*weight+0.518*height=" + pca + ", eyeColor=" + eyeColor
				+ ", hairColor=" + hairColor + ", region=" + region + "]";
	}
	
}
