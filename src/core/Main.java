package core;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main {

	public static Person sample1 = new Person("Jack", 145.189, "brown", "black", "?");
	public static Person sample2 = new Person("Todd", 159.437, "blue", "blond", "?");
	
	public static ArrayList<Person> persons = new ArrayList<>();
	
	public static void main(String[] args) {
		// Initialize Data
		ArrayList<Person> persons = new ArrayList<>();
		persons.add(new Person("Abel", 155.655, "hazel", "black", "Europe"));
		persons.add(new Person("Bob", 158.919, "brown", "brown", "Europe"));
		persons.add(new Person("Carl", 151.018, "hazel", "blond", "Europe"));
		persons.add(new Person("Dale", 138.324, "brown", "black", "Asia"));
		persons.add(new Person("Eric", 145.707, "brown", "brown", "Asia"));
		persons.add(new Person("Felix", 138.505, "brown", "blond", "Asia"));
		persons.add(new Person("George", 159.93, "blue", "black", "America"));
		persons.add(new Person("Howard", 161.122, "hazel", "brown", "America"));
		persons.add(new Person("Igor", 154.775, "blue", "blond", "America"));
		
		System.out.println("Input data: ");
		for(Person person : persons){
			System.out.println(person);
		}
		System.out.println();
		
		// output prior probabilities for each class
		ArrayList<Person> europeans = new ArrayList<>();
		ArrayList<Person> asians = new ArrayList<>();
		ArrayList<Person> americans = new ArrayList<>();
		
		for(Person person : persons){
			if(person.region.equals("Europe"))
				europeans.add(person);
			else if(person.region.equals("Asia"))
				asians.add(person);
			else if(person.region.equals("America"))
				americans.add(person);
		}
		
		double priorProb_europe = (double)europeans.size() / (double)persons.size();
		double priorProb_asia = (double)asians.size() / (double)persons.size();
		double priorProb_america = (double)americans.size() / (double)persons.size();
		System.out.println("Prior probability for each class:");
		System.out.println("Class Europe	= " + priorProb_europe);
		System.out.println("Class Asia	= " + priorProb_asia);
		System.out.println("Class America	= " + priorProb_america);
		System.out.println();
		
		System.out.println("Predicts region for following 2 samples:");
		System.out.println(sample1);
		System.out.println(sample2);
		System.out.println();
		
		// output the value of P(sample | Ci)
		double likelihood_sample1_europe = getLikelihood(europeans, sample1);
		double likelihood_sample1_asia = getLikelihood(asians, sample1);
		double likelihood_sample1_america = getLikelihood(americans, sample1);
		
		double likelihood_sample2_europe = getLikelihood(europeans, sample2);
		double likelihood_sample2_asia = getLikelihood(asians, sample2);
		double likelihood_sample2_america = getLikelihood(americans, sample2);
		
		System.out.println("P(sample1|region=Europe) = " + String.format("%.7f", likelihood_sample1_europe) );
		System.out.println("P(sample1|region=Asia) = " + String.format("%.7f", likelihood_sample1_asia) );
		System.out.println("P(sample1|region=America) = " + String.format("%.7f", likelihood_sample1_america) );
		System.out.println();
		System.out.println("P(sample2|region=Europe) = " + String.format("%.7f", likelihood_sample2_europe) );
		System.out.println("P(sample2|region=Asia) = " + String.format("%.7f", likelihood_sample2_asia) );
		System.out.println("P(sample2|region=America) = " + String.format("%.7f", likelihood_sample2_america) );
		System.out.println();
		
		// output the predicted region for each test sample
		if(likelihood_sample1_europe > likelihood_sample1_asia
				&& likelihood_sample1_europe > likelihood_sample1_america){
			System.out.println("Predicted region of sample1 is Europe");
		}
		else if(likelihood_sample1_asia > likelihood_sample1_america
				&& likelihood_sample1_asia > likelihood_sample1_europe){
			System.out.println("Predicted region of sample1 is Asia");
		}
		else{
			System.out.println("Predicted region of sample1 is America");
		}
		
		if(likelihood_sample2_europe > likelihood_sample2_asia
				&& likelihood_sample2_europe > likelihood_sample2_america){
			System.out.println("Predicted region of sample2 is Europe");
		}
		else if(likelihood_sample2_asia > likelihood_sample2_america
				&& likelihood_sample2_asia > likelihood_sample2_europe){
			System.out.println("Predicted region of sample2 is Asia");
		}
		else{
			System.out.println("Predicted region of sample2 is America");
		}
	}

	public static double getLikelihood(ArrayList<Person> persons, Person sample){
		
		double pcaMean = getMean(persons);
		double pcaSd = getStdDev(persons);
		
		double eyeBrownCount = 0.0;
		double hairBrownCount = 0.0;
		for(Person person : persons){
			if(person.eyeColor.equals(sample.eyeColor)){
				++eyeBrownCount;
			}
			if(person.hairColor.equals(sample.hairColor)){
				++hairBrownCount;
			}
		}
		
		double prob_sample_pca = NormalDist(pcaMean, pcaSd, sample.pca);

		double prob_sample_Likelihood =
				prob_sample_pca *
				(eyeBrownCount+1) / (double)(persons.size() + 3) * // Laplacian correction
				(hairBrownCount+1) / (double)(persons.size() + 3); // Laplacian correction
		
		return prob_sample_Likelihood;
	};
	
	public static double NormalDist(double mean, double sd, double x){
		return Math.exp( - Math.pow( (x - mean) / sd, 2) / 2 ) 
				/ (Math.sqrt(2 * Math.PI) * sd);
	}
	
	public static double getMean(ArrayList<Person> persons)
    {
        double sum = 0.0;
        for(Person person : persons){
        	sum += person.pca;
        }
        return sum / persons.size();
    }

	public static double getVariance(ArrayList<Person> persons)
    {
        double mean = getMean(persons);
        double temp = 0;
        for(Person person : persons){
        	temp += (person.pca-mean)*(person.pca-mean);
        }
        return temp / (persons.size()-1);
    }

	public static double getStdDev(ArrayList<Person> persons)
    {
        return Math.sqrt(getVariance(persons));
    }
    
}
